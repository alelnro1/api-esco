<?php

namespace App\Classes\Api;

use App\Classes\SPBuilders\SPBuilderFactory;
use App\Http\Controllers\Api\BaseController;
use Illuminate\Database\QueryException;

class RunWS
{
    /**
     * Ejecutamos el webservice si las validaciones son correctas.
     * Si ya existe el base controller significa que no es la primera peticion, por lo que
     *  en caso de que falle, se deberá hacer un rollback completo de todas las transacciones
     *  hechas hasta el momento del error
     *
     * Si recibimos una key es porque vamos a ejecutar un bloque de SP. Ej: En titulos
     *
     * @param $request
     * @param $validator
     * @param $base
     * @param $sp_required
     * @param $key
     * @return \Illuminate\Http\JsonResponse
     */
    public function execute($request, $validator, $base, $sp_required, $key = false, $end_transaction)
    {
        $validaciones = $this->validar($validator);

        if ($validaciones !== true) {
            if (!$base) {
                return response()->json([
                    'errores' => $validaciones
                ], 422);
            } else {
                $base->desconectarDB();
            }
        }

        // Vamos a hacer la primera conexion a la base
        
        if (!$base) {
            // Defino que se está realizando una solicitud única
            $solicitud_unica = true;

            // Obtengo la instancia del controller base
            $base = BaseController::getInstance();

            // Me conecto a la base
            $db = $base->conectarDB($request);
            
            if (!$db) {
                return response()->json([
                    'error' => 'Login invalido.'
                ], 401);
            }
        } else {
            $solicitud_unica = false;

            $base = BaseController::getInstance();

            $db = $base->probarConexion($request->username_db, $request->password_db);
        }

        try {
            // Voy a armar el SP con los datos que me pasaron
            $sp_factory = SPBuilderFactory::create($sp_required);
            $sp = $sp_factory->buildSP($base, $request, $key);


            //return response()->json($sp);

            // Ejecuto el SP
            $resultado = $db->select($sp);

            // Agrego el resultado al array de resultados global, contenido en el Singleton

            $base->agregarNuevoResultado($resultado);
        } catch (QueryException $e) {
            return $base->desconectarDB($e);
        }

        // Si me solicitaron hacer solo esta accion => me desconecto de la base y hago un commit/rollback
     
        if ($end_transaction) {
            // Me conecto a la base
            return $base->desconectarDB();
        }
    }

    public function validar($validator)
    {
        $validator->validate();

        if ($validator->fails()) {
            return $validator->messages()->all();
        } else {
            return true;
        }
    }
}