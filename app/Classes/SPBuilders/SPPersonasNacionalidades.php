<?php

namespace App\Classes\SPBuilders;

use App\Traits\ParsearValores;

class SPPersonasNacionalidades extends SPBuilder
{
    use ParsearValores;

    public function buildSP($base, $request, $key = false)
    {

        $sp = "SET NOCOUNT ON; EXEC INTL_TR_PERSNACIONALIDAD ";
    
        // Si viene seteado el parametro se agrega, son los parametros obligatorios

        if (isset($request->cod_persona))
        {
            $sp .= "@CodPersona=" . $this->parsearValor($request->cod_persona) . ",";
        }

        if (isset($request->num_persona))
        {
            $sp .= "@NumPersona=" . $this->parsearValor($request->num_persona) . ",";
        }

        if (isset($request->num_documento))
        {
            $sp .= "@NumDocumento=" . $this->parsearValor($request->num_documento,true) . ",";
        }

        if (isset($request->cod_persona_nacionalidad))
        {
            $sp .= "@CodPersNacionalidad=" . $this->parsearValor($request->cod_persona_nacionalidad,true) . ",";
        }

        if (isset($request->cod_pais))
        {
            $sp .= "@CodPais=" . $this->parsearValor($request->cod_pais,true) . ",";
        }


        $sp .= "@CodAccion=" . $this->parsearValor($request->cod_accion, true);

        return $sp;
    }
}