<?php

namespace App\Classes\SPBuilders;

use App\Traits\ParsearValores;

class SPTransferenciaTitulos extends SPBuilder
{
    use ParsearValores;

    public function buildSP($base, $request, $key = false)
    {
        $sp = "SET NOCOUNT ON; EXEC INTL_TR_TRANSFERENCIASTITMOV ";

        if (isset($request->titulos[$key]['abreviatura']))
        {
            $sp .= "@Abreviatura=" . $this->parsearValor($request->titulos[$key]['abreviatura']) . ",";
        }

        if (isset($request->titulos[$key]['cod_transferencia_tit_mov']))
        {
            $sp .= "@CodTransferenciaTitMov=" . $this->parsearValor($request->titulos[$key]['cod_transferencia_tit_mov']) . ",";
        }

        if (isset($request->titulos[$key]['cod_especie']))
        {
            $sp .= "@CodEspecie=" . $this->parsearValor($request->titulos[$key]['cod_especie']) . ",";
        }

        if (isset($request->titulos[$key]['cantidad'])) {
            $sp .= "@Cantidad=" . $this->parsearValor($request->titulos[$key]['cantidad']) . ",";
        }

        /*if (isset($request->titulos[$key]['observaciones'])) {
            $sp .= "@Observaciones=" . $this->parsearValor($request->titulos[$key]['observaciones']) . ",";
        }

        if (isset($request->titulos[$key]['cod_pais_tit'])) {
            $sp .= "@CodPaisTit=" . $this->parsearValor($request->titulos[$key]['cod_pais_tit']) . ",";
        }

        if (isset($request->titulos[$key]['es_transferencia_tit']) && $request->titulos[$key]['es_transferencia_tit'] == "Y")
        {
            $sp .= "@YNEsTransferenciaTit=" . $this->parsearValor($request->titulos[$key]['es_transferencia_tit']) . ",";
        }

        if (isset($request->titulos[$key]['se_informa_transf_cust']) && $request->titulos[$key]['se_informa_transf_cust'] == "Y")
        {
            $sp .= "@YNSeInformaTransfCust=" . $this->parsearValor($request->titulos[$key]['se_informa_transf_cust']) . ",";
        }

        if (isset($request->titulos[$key]['esta_emitida_transf_cust']) && $request->titulos[$key]['esta_emitida_transf_cust'] == "Y")
        {
            $sp .= "@YNEstaEmitidaTransfCust=" . $this->parsearValor($request->titulos[$key]['esta_emitida_transf_cust']) . ",";
        }

        if (isset($request->titulos[$key]['es_manual']) && $request->titulos[$key]['es_manual'] == "N")
        {
            $sp .= "@YNEsManual=" . $this->parsearValor($request->titulos[$key]['es_manual']) . ",";
        }

        */

        if (isset($request->num_comitente_hst)) {
            $sp .= "@NumComitenteHst=" . $this->parsearValor($request->num_comitente_hst) . ",";
        }

        $valores_opcionales = [
            "Precio" => $this->parsearValor($request->precio),
            "CodPaisTit" => $this->parsearValor($request->cod_pais_tit),
            "Observaciones" => $this->parsearValor($request->observaciones),
            "YNEsTransferenciaTit" => $this->parsearValor($request->es_transferencia_tit, true, 'Y'),
            "YNSeInformaTransfCust" => $this->parsearValor($request->se_informa_transf_cust, true, 'Y'),
            "YNEstaEmitidaTransfCust" => $this->parsearValor($request->esta_emitida_transf_cust, true, 'Y'),
            "YNEsManual" => $this->parsearValor($request->es_manual, true, 'N')
        ];

        $sp .= $this->armarValoresOpcionales($valores_opcionales);

        $sp .=
            "@CodAgenteDepoDde=" . $this->parsearValor($request->cod_agente_depo_dde) . ",
            @CodAgenteDepoHst=" . $this->parsearValor($request->cod_agente_depo_hst) . ",
            @CodComitenteHst=" . $this->parsearValor($request->cod_comitente_hst) . ",
            @CodComitenteDde=" . $this->parsearValor($request->cod_comitente_dde) . ",
            @NumComitenteDde=" . $this->parsearValor($request->num_comitente_dde) . ",
            @CodTpTransferenciaTitMov=" . $this->parsearValor($request->cod_tp_transferencia_tit_mov) . ",
            @FechaLiquidacion=". $this->parsearValor($request->fecha_liquidacion, true) . ",
            @FechaConcertacion=" . $this->parsearValor($request->fecha_concertacion, true) . ",";

        $sp .= "@CodAccion=" . $this->parsearValor($request->cod_accion, true);

        return $sp;
    }
}