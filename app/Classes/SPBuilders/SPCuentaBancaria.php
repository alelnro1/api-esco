<?php

namespace App\Classes\SPBuilders;

use App\Traits\ParsearValores;

class SPCuentaBancaria extends SPBuilder
{
    use ParsearValores;

    public function buildSP($base, $request, $key = false)
    {
        $sp =
            "SET NOCOUNT ON; EXEC INTL_TR_CMTCTASENTLIQUIDACIONS
                @NumComitente=" . $this->parsearValor($request->num_comitente) . ",";

        /*if (isset($request->es_liquidadora) && $request->es_liquidadora != "") {
            $sp .= "@YNEsLiquidadora=" . $this->parsearValor($request->es_liquidadora) . ",";
        }

        if (isset($request->es_default) && $request->es_default != "") {
            $sp .= "@YNEsDefault=" . $this->parsearValor($request->es_default) . ",";
        }

        if (isset($request->usa_param_ent_liq) && $request->usa_param_ent_liq != "") {
            $sp .= "@YNUsaParamEntLiq=" . $this->parsearValor($request->usa_param_ent_liq) . ",";
        }*/

        $valores_opcionales = [
            "CBU" => $this->parsearValor($request->cbu, true),
            "Alias" => $this->parsearValor($request->alias, true),
            "CuitTitular" => $this->parsearValor($request->cuit_titular, true),
            "NumSucursal" => $this->parsearValor($request->num_sucursal),
            "YNPermiteParc" => $this->parsearValor($request->permite_parc, true, 'Y'),
            "YNUsaParamEntLiq" => $this->parsearValor($request->usa_param_ent_liq, true),
            "YNEsDefault" => $this->parsearValor($request->es_default, true),
            "YNEsLiquidadora" => $this->parsearValor($request->es_liquidadora, true, 'Y')
        ];

        $sp .= $this->armarValoresOpcionales($valores_opcionales);

        $sp .=
            "@CodInterfazMoneda=" . $this->parsearValor($request->cod_interfaz_moneda, true) . ",
            
            @CodAccion='" . $request->cod_accion . "',
            @CodComitente=" . $this->parsearValor($request->cod_comitente) . ",
            @CodCmtCtaEntLiquidacion=" . $this->parsearValor($request->cod_cmt_cta_liquidacion) . ",
            
            @CodEntLiquidacion=" . $this->parsearValor($request->cod_ent_liquidacion) . ",
            @CodTpCtaBancaria=" . $this->parsearValor($request->cod_tp_cta_bancaria) . ",
            @CodMoneda=" . $this->parsearValor($request->cod_moneda) . ",
            @FechaApertura=" . $this->parsearValor($request->fecha_apertura, true) . ",
            @NumSubCuentaMon=" . $this->parsearValor($request->num_sub_cuenta_mon, true) . ";  
                              
            SELECT TOP 1 CodCmtCtaEntLiquidacion FROM CMTCTASENTLIQUIDACION  where NumSubCuentaMon='" . $request->num_sub_cuenta_mon . "' order by FechaApertura desc
            ";

        return $sp;
    }
}