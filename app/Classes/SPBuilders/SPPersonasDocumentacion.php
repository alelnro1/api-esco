<?php

namespace App\Classes\SPBuilders;

use App\Traits\ParsearValores;

class SPPersonasDocumentacion extends SPBuilder
{
    use ParsearValores;

    public function buildSP($base, $request, $key = false)
    {

        $sp = "SET NOCOUNT ON; EXEC INTL_TR_PERSDOCUMENTACION ";
    
        // Si viene seteado el parametro se agrega, son los parametros obligatorios

        if (isset($request->cod_persona))
        {
            $sp .= "@CodPersona=" . $this->parsearValor($request->cod_persona) . ",";
        }

        if (isset($request->num_persona))
        {
            $sp .= "@NumPersona=" . $this->parsearValor($request->num_persona) . ",";
        }

        if (isset($request->num_documento))
        {
            $sp .= "@NumDocumento=" . $this->parsearValor($request->num_documento,true) . ",";
        }

        if (isset($request->cod_documentacion))
        {
            $sp .= "@CodDocumentacion=" . $this->parsearValor($request->cod_documentacion,true) . ",";
        }


        $valores_opcionales = [
            "YNFuePresentado" => $this->parsearValor($request->fue_presentado),
            "FechaVencimiento" => $this->parsearValor($request->fecha_vencimiento, true)
        ];

        $sp .= $this->armarValoresOpcionales($valores_opcionales);

        $sp .= "@CodAccion=" . $this->parsearValor($request->cod_accion, true);

        return $sp;
    }
}