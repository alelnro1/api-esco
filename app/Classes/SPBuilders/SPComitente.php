<?php

namespace App\Classes\SPBuilders;

use App\Traits\ParsearValores;

class SPComitente extends SPBuilder
{
    use ParsearValores;

    /**
     * @param $base
     * @param $request
     * @param bool $key
     * @return string
     */
    public function buildSP($base, $request, $key = false)
    {
        $sp =
            "SET NOCOUNT ON; EXEC INTL_TR_COMITENTES
                @CodAccion=" . $this->parsearValor($request->cod_accion, true) . ",";

        if ($request->nivel_seguridad != 0)
        {
            $sp .= "@NivelSeguridad=" . $this->parsearValor($request->nivel_seguridad) . ",";
        }

        $sp .= "@FechaIngreso=" . $this->parsearValor($request->fecha_ingreso, true) . ",";

        $valores_opcionales = [
            "CodTpAfjpCmt"              => $this->parsearValor($request->cod_tp_afjp_cmt, true),
            "CodGrupoEco"               => $this->parsearValor($request->cod_grupo_eco),
            "CodTpManejoCart"           => $this->parsearValor($request->cod_tp_manejo_cart, true),
            "CodCanalVta"               => $this->parsearValor($request->cod_canal_vta),
            "CodCategoria"              => $this->parsearValor($request->cod_categoria),
            "ReferenciaFirmaConjunta"   => $this->parsearValor($request->referencia_firma_conjunta),
            "CodTpCmtTrading"           => $this->parsearValor($request->cod_tp_cmt_trading),
            "YNCodPRI"                  => $this->parsearValor($request->cod_pri, true, 'Y'),
            "CodAgente"                 => $this->parsearValor($request->cod_agente),
            "CodCuotapartista"          => $this->parsearValor($request->cod_cuotapartista),
            "Contacto"                  => $this->parsearValor($request->contacto, true),
            "Observaciones"             => $this->parsearValor($request->observaciones, true),
            "EmailsInfo"                => $this->parsearValor($request->emails_info, true),
            "YNNoPresencial"            => $this->parsearValor($request->no_presencial, true, 'Y'),
            "YNFondosTerceros"          => $this->parsearValor($request->fondos_terceros, true, 'N'),
            "CodCategoriaUIF"           => $this->parsearValor($request->cod_categoria_uif),
            "YNEsAsignCatUIFManual"     => $this->parsearValor($request->es_asign_cat_uif_manual, true, 'Y'),
            "YNEsInstitucional"         => $this->parsearValor($request->es_institucional, true, 'Y'),
            "YNEsCarteraPropia"         => $this->parsearValor($request->es_cartera_propia, true, 'N'),
            "YNEsMercado"               => $this->parsearValor($request->es_mercado, true, 'Y'),
            "YNRequiereFirmaConjunta"   => $this->parsearValor($request->require_firma_conjunta, true, 'Y'),
            "YNEsFisico"                => $this->parsearValor($request->es_fisico, true, 'Y')
        ];

        $sp .= $this->armarValoresOpcionales($valores_opcionales);

        $sp .=
                "@CodComitente=" . $this->parsearValor($request->cod_comitente) . ",
                @CodGrupoArOperBurs=" . $this->parsearValor($request->cod_grupo_ar_opers_burs) . ",
                @CodTpContribRetencion=" . $this->parsearValor($request->cod_tp_contrib_retencion, true) . ",
                @CodGrupoArAcr=" . $this->parsearValor($request->cod_grupo_acr) . ",
                @Descripcion=" . $this->parsearValor($request->descripcion, true) . ",
                @NumComitente=" . $this->parsearValor($request->num_comitente);

        return $sp;
    }
}