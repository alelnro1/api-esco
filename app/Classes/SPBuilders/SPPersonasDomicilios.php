<?php

namespace App\Classes\SPBuilders;

use App\Traits\ParsearValores;

class SPPersonasDomicilios extends SPBuilder
{
    use ParsearValores;

    public function buildSP($base, $request, $key = false)
    {

        $sp = "SET NOCOUNT ON; EXEC INTL_TR_PERSDOMICILIOS ";
    
        // Si viene seteado el parametro se agrega, son los parametros obligatorios


        if (isset($request->cod_persona))
        {
            $sp .= "@CodPersona=" . $this->parsearValor($request->cod_persona) . ",";
        }

        if (isset($request->num_persona))
        {
            $sp .= "@NumPersona=" . $this->parsearValor($request->num_persona) . ",";
        }

        if (isset($request->num_documento))
        {
            $sp .= "@NumDocumento=" . $this->parsearValor($request->num_documento,true) . ",";
        }

        if (isset($request->cod_tp_domicilio))
        {
            $sp .= "@CodTpDomicilio=" . $this->parsearValor($request->cod_tp_domicilio,true) . ",";
        }

        if (isset($request->cod_pais))
        {
            $sp .= "@CodPais=" . $this->parsearValor($request->cod_pais,true) . ",";
        }

        if (isset($request->cod_provincia))
        {
            $sp .= "@CodProvincia=" . $this->parsearValor($request->cod_provincia,true) . ",";
        }

        if (isset($request->calle))
        {
            $sp .= "@Calle=" . $this->parsearValor($request->calle,true) . ",";
        }

        if (isset($request->altura_calle))
        {
            $sp .= "@AlturaCalle=" . $this->parsearValor($request->altura_calle,true) . ",";
        }

        if (isset($request->codigo_postal))
        {
            $sp .= "@CodigoPostal=" . $this->parsearValor($request->codigo_postal,true) . ",";
        }

        if (isset($request->localidad))
        {
            $sp .= "@Localidad=" . $this->parsearValor($request->localidad,true) . ",";
        }

        $valores_opcionales = [
            "Piso" => $this->parsearValor($request->piso),
            "Departamento" => $this->parsearValor($request->departamento,true),
            "Telefono" => $this->parsearValor($request->telefono,true),
            "Fax" => $this->parsearValor($request->fax,true),
            "Sector" => $this->parsearValor($request->sector,true),
            "Torre" => $this->parsearValor($request->torre,true),
            "Manzana" => $this->parsearValor($request->manzana,true)
        ];

        $sp .= $this->armarValoresOpcionales($valores_opcionales);


        $sp .= "@CodAccion=" . $this->parsearValor($request->cod_accion, true);

        return $sp;
    }
}