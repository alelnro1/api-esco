<?php

namespace App\Classes\SPBuilders;

abstract class SPBuilder
{
    public abstract function buildSP($base, $request, $key = false);
}