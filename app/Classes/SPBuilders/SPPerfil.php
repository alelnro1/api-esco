<?php

namespace App\Classes\SPBuilders;

use App\Traits\ParsearValores;

class SPPerfil extends SPBuilder
{
    use ParsearValores;

    public function buildSP($base, $request, $key = false)
    {
        $sp = "SET NOCOUNT ON; EXEC INTL_TR_PERFILESCMT ";

        $valores_opcionales = [
            "CodComitente"  => $this->parsearValor($request->cod_comitente, true),
            "NumComitente"  => $this->parsearValor($request->num_comitente),
            "FechaPerfil"   => $this->parsearValor($request->fecha_perfil, true),
        ];

        $sp .= $this->armarValoresOpcionales($valores_opcionales);

        $sp .= "@Valor=" . $this->parsearValor($request->valor);

        return $sp;
    }
}