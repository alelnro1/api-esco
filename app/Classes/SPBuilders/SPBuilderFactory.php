<?php

namespace App\Classes\SPBuilders;

use Illuminate\Support\Facades\Log;

class SPBuilderFactory
{
    private static $prefix = "SP";

    public static function create($sp)
    {
        $clase = "App\Classes\SPBuilders\\" . self::$prefix . $sp;

        if (class_exists($clase)) {
            return new $clase();
        }

        Log::critical('SPBuilderFactory. La clase ' . $clase . ' no existe');

        return false;
    }
}