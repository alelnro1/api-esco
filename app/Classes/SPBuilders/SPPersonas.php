<?php

namespace App\Classes\SPBuilders;

use App\Traits\ParsearValores;

class SPPersonas extends SPBuilder
{
    use ParsearValores;

    public function buildSP($base, $request, $key = false)
    {

        $sp = "SET NOCOUNT ON; EXEC INTL_TR_PERSONAS ";
    
        // Si viene seteado el parametro se agrega, son los parametros obligatorios

        if (isset($request->cod_persona))
        {
            $sp .= "@CodPersona=" . $this->parsearValor($request->cod_persona) . ",";
        }

        if (isset($request->num_persona))
        {
            $sp .= "@NumPersona=" . $this->parsearValor($request->num_persona) . ",";
        }

        if (isset($request->num_documento))
        {
            $sp .= "@NumDocumento=" . $this->parsearValor($request->num_documento,true) . ",";
        }

        if (isset($request->nombre))
        {
            $sp .= "@Nombre=" . $this->parsearValor($request->nombre,true) . ",";
        }

        if (isset($request->apellido))
        {
            $sp .= "@Apellido=" . $this->parsearValor($request->apellido,true) . ",";
        }

        if (isset($request->descripcion))
        {
            $sp .= "@Descripcion=" . $this->parsearValor($request->descripcion,true) . ",";
        }

        if (isset($request->cod_tipo_contribucion_iva))
        {
            $sp .= "@CodTpContribIVA=" . $this->parsearValor($request->cod_tipo_contribucion_iva,true) . ",";
        }

        if (isset($request->cod_documento_identidad))
        {
            $sp .= "@CodDocIdentidad=" . $this->parsearValor($request->cod_documento_identidad,true) . ",";
        }

        if (isset($request->es_fisico))
        {
            $sp .= "@YNEsFisico=" . $this->parsearValor($request->es_fisico,true) . ",";
        }

        if (isset($request->cod_sexo))
        {
            $sp .= "@CodSexo=" . $this->parsearValor($request->cod_sexo,true) . ",";
        }

        if (isset($request->fecha_ingreso))
        {
            $sp .= "@FechaIngreso=" . $this->parsearValor($request->fecha_ingreso,true) . ",";
        }

        if (isset($request->fecha_nacimiento))
        {
            $sp .= "@FechaNacimiento=" . $this->parsearValor($request->fecha_nacimiento,true) . ",";
        }

        if (isset($request->email))
        {
            $sp .= "@Email=" . $this->parsearValor($request->email,true) . ",";
        }

        $valores_opcionales = [
            'LugarNacimiento' => $this->parsearValor($request->lugar_nacimiento, true),
            'CUIL' => $this->parsearValor($request->cuil, true),
            'CUIT' => $this->parsearValor($request->cuit, true),
            'CDI' => $this->parsearValor($request->cdi, true),
            'RUC' => $this->parsearValor($request->ruc, true),
            'EsBeneficiario' => $this->parsearValor($request->es_beneficiario, true),
            'EsCliente' => $this->parsearValor($request->es_cliente, true),
            'EsClienteEspecial' => $this->parsearValor($request->es_cliente_especial, true),
            'PEPSObliga' => $this->parsearValor($request->peps_boliga, true),
            'PEPSBaja' => $this->parsearValor($request->peps_baja, true),
            'NoPresencial' => $this->parsearValor($request->no_presencial, true),
            'EsAgenteRecaudador' => $this->parsearValor($request->es_agente_recaudador, true),
            'NumInscripcion' => $this->parsearValor($request->num_inscripcion, true),
            'EsSujetoObligado' => $this->parsearValor($request->es_sujeto_obligado, true),
            'CodTpIdFatca' => $this->parsearValor($request->codigo_fatca, true),
            'IdFatca' => $this->parsearValor($request->id_fatca, true),
            'EsInversorCalificado' => $this->parsearValor($request->es_inversor_calificado, true),
            'PerteneceLUT' => $this->parsearValor($request->pertenece_lut, true),
            'CodPerfilesEcoFin' => $this->parsearValor($request->cod_perfiles_eco_fin, true),
            'EsExtranjero' => $this->parsearValor($request->es_extranjero, true),
            'CodActividad' => $this->parsearValor($request->cod_actividad, true),
            'CodPaisNacionalidad' => $this->parsearValor($request->cod_pais_nacionalidad, true),
            'CodTpContribIngBrutos' => $this->parsearValor($request->cod_tipo_ingresos_brutos, true),
            'CodUsuarioAuditoria' => $this->parsearValor($request->cod_usuario_auditoria, true),
        ];

        $sp .= $this->armarValoresOpcionales($valores_opcionales);

        $sp .= "@CodAccion=" . $this->parsearValor($request->cod_accion, true);

    
           return $sp;
    }
}