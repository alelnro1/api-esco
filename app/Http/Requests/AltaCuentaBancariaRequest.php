<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;

class AltaCuentaBancariaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function response(array $errors)
    {
        if ($this->expectsJson()) {
            return new JsonResponse(['status' => 2, 'msg' => $errors], 422);
        }

        return parent::response($errors);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username_db' => 'required|string',
            'password_db' => 'required|string',

            'cod_accion' => 'required|in:A,M,D,E,R',

            'cod_comitente' => 'integer|required_without:num_comitente|empty_when:num_comitente',
            'num_comitente' => 'integer|required_without:cod_comitente|empty_when:cod_comitente',

            'cod_cmt_cta_liquidacion' => 'integer',
            //'cod_tp_cta_bancaria' => 'integer',
            'cod_moneda' => 'integer',
            'fecha_apertura' => 'string',
            'num_sub_cuenta_mon' => 'string',

            'cbu' => 'string',
            'alias' => 'string',
            'cuit_titular' => 'string',
            'num_sucursal' => 'integer',
            'es_liquidadora' => 'in:Y,N',
            'es_default' => 'in:Y,N',
            'usa_param_ent_liq' => 'in:Y,N',
            'permite_parc' => 'in:Y,N'
        ];
    }

    public function withValidator($validator)
    {
        // Validamos que se reciba el cod de cuenta bancaria siempre y cuando no sea un ALTA
        $validator->sometimes('cod_cmt_cta_liquidacion', 'required', function () {
            return $this->cod_accion != "A";
        });

        // Validamos que nos pasen los datos necesarios cuanto es un ALTA o una MODIFICACION
        if ($this->cod_accion == "A" || $this->cod_accion == "M") {
            $validator->addRules([
                'cod_interfaz_moneda' => 'required_without:cod_moneda|empty_when:cod_moneda',
                'cod_moneda' => 'required_without:cod_interfaz_moneda|empty_when:cod_interfaz_moneda',

                'cod_ent_liquidacion' => 'required',
                'cod_tp_cta_bancaria' => 'required',
                'fecha_apertura' => 'required',
                'num_sub_cuenta_mon' => 'required'
            ]);
        }

        return $validator->validate();
    }
}
