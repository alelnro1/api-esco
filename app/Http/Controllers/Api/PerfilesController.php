<?php

namespace App\Http\Controllers\Api;

use App\Classes\Api\RunWS;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PerfilesController extends RunWS
{
    /**
     * Mando a ejecutar el webservice
     *
     * @param null $base
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function run($base = null, Request $request)
    {
        $validator = $this->getValidator($request);

        return $this->execute($request, $validator, $base, 'Perfil');
    }

    protected function getValidator($request)
    {
        $validator = Validator::make($request->all(), [
            'username_db' => 'required|string',
            'password_db' => 'required|string'
        ]);

        $validator->after(function ($validator) use ($request) {
            $validator->addRules([
                'cod_comitente' => 'integer|required_without:num_comitente|empty_when:num_comitente',
                'num_comitente' => 'integer|required_without:cod_comitente|empty_when:cod_comitente',
                'valor'         => 'required|numeric'
            ]);
        });

        $validator->after(function ($validator) use ($request) {
            if ($request->fecha_perfil != "" && $request->fecha_perfil != date("Ymd", time())) {
                $validator->addRules([
                    'fecha_perfil' => 'date_format:"Ymd"',
                ]);
            }
        });


        return $validator;
    }
}
