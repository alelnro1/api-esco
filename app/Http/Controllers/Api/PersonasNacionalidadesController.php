<?php

namespace App\Http\Controllers\Api;

use App\Classes\Api\RunWS;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

/**
 * Class TransferenciaTitulosController
 * @package App\Http\Controllers\Api
 *
 * @api {post} /transferencia-titulos Operaciones
 * @apiGroup Transferencia de Titulos
 * @apiDescription Este servicio permite realizar transferencias de titulos (nacionales e internacionales)
 * @apiVersion 0.1.0
 * @apiUse Login
 *
 * @apiParam {Number} cod_comitente_dde El código del comitente originario. En caso de no conocerlo, se deberá especificar el <code>num_comitente</code> originario
 * @apiParam {Number} [num_comitente_dde] El número del comitente originario. Debe especificarse <b>únicamente</b> en caso de no saber el cod_comitente originario
 *
 * @apiParam {Number} cod_comitente_hst El código del comitente destinatario. En caso de no conocerlo, se deberá especificar el <code>num_comitente</code> destinatario
 * @apiParam {Number} [num_comitente_hst] El número del comitente destinatario. Debe especificarse <b>únicamente</b> en caso de no saber el cod_comitente  destinatario
 *
 * @apiParam {Char} cod_accion La acción a realizar. <code>A</code>: alta | <code>M</code>: modificar | <code>D</code>: detalle | <code>E</code>: eliminar | <code>R</code>: restaurar
 * @apiParam {Number} cod_agente_depo_dde El código de agente originario
 * @apiParam {Number} cod_agente_depo_hst El código de agente destinario
 * @apiParam {String} fecha_concertacion La fecha de concertacion de la transferencia de titulos
 * @apiParam {String} fecha_liquidacion La fecha de liquidacion de la transferencia de titulos
 *
 * @apiParam {Number} [precio]
 * @apiParam {Number} [cod_pais_tit]
 * @apiParam {String} [observaciones]
 * @apiParam {Char} [es_transferencia_tit] Valores válidos: <code>Y</code> | <code>N</code>
 * @apiParam {Char} [se_informa_transf_cust] Valores válidos: <code>Y</code> | <code>N</code>
 * @apiParam {Char} [esta_emitida_transf_cust] Valores válidos: <code>Y</code> | <code>N</code>
 * @apiParam {Char} [es_manual] Valores válidos: <code>Y</code> | <code>N</code>
 * @apiParam {String} cod_tp_transferencia_tit_mov <code>EC</code> Recepción | <code>Envío</code
 *
 * @apiParam {Number} cantidad La cantidad de titulos a transferir
 * @apiParam {Array} titulos Objeto titulos
 * @apiParam {Number} titulos.cod_especie El código de la especie. En caso de no conocerlo, se deberá especificar la <code>abreviatura</code> de la especie
 * @apiParam {String} [titulos.abreviatura] La abreviatura de la especie. Debe especificarse <b>únicamente</b> en caso de no saber el cod_especie
 * @apiParam {Number} [titulos.cod_transferencia_tit_mov] Obligatorio en todos <u>menos</u> <code>A</code>
 * @apiParam {Number} titulos.cantidad La cantidad de titulos a transferir
 */

class PersonasNacionalidadesController extends RunWS
{
    public function run($base = null, Request $request)
    {

        $validator = $this->getValidator($request);
        
        return $this->execute($request, $validator, $base, 'PersonasNacionalidades');
    }

    /**
     * Defino las validaciones
     *
     * @param $request
     * @return mixed
     */
    private function getValidator($request)
    {
        $validator = Validator::make($request->all(), [
            'username_db' => 'required|string',
            'password_db' => 'required|string'
        ]);

        // Valido que reciba lo minimo indispensable para saber que validaciones voy a ejecutar (codigo accion, titulos y tipo de transferencia)
        $validator->after(function ($validator) use ($request) {
            $validator->addRules([
                'cod_accion' => 'required|in:A,M,D,E,R'
            ]);
        });

        $validator->after(function ($validator) use ($request) {
            $validator->addRules([
                'cod_persona' => 'integer',
                'num_persona' => 'integer',
                'num_documento' => 'string',
                'cod_persona_nacionalidad' => 'string',
                'cod_pais' => 'string'
            ]);
        });

        // Agrego las validaciones exclusivas para ALTA o MODIFICACION
        $validator->after(function ($validator) use ($request) {
            if ($request->cod_accion == "M") {
                $validator->addRules([
                    'cod_persona_nacionalidad' => 'string|string|min:1|required',
                ]);
            }
        });

        $validator->after(function ($validator) use ($request) {
            if ($request->cod_accion == "A" || $request->cod_accion == "M") {
                $validator->addRules([
                    'cod_persona' => 'integer|integer|min:1|required_without_all:num_documento,num_persona',
                    'num_documento' => 'string|string|min:1|required_without_all:cod_persona,num_persona',
                    'num_persona' => 'integer|integer|min:1|required_without_all:cod_persona,num_documento',
                    'cod_pais' => 'required|string'
                ]);
            }
        });

        // Agrego las validaciones exclusivas para DETALLE, ELIMINAR Y RESTORE
        $validator->after(function ($validator) use ($request) {
            if ($request->cod_accion == "D" || $request->cod_accion == "E" || $request->cod_accion == "R") {
                $validator->addRules([
                    'cod_persona' => 'integer|integer|min:1|required_without_all:num_documento,num_persona,cod_persona_nacionalidad',
                    'num_documento' => 'string|string|min:1|required_without_all:cod_persona,num_persona,cod_persona_nacionalidad',
                    'num_persona' => 'integer|integer|min:1|required_without_all:cod_persona,num_documento,cod_persona_nacionalidad',
                    'cod_persona_nacionalidad' => 'string|string|min:1|required_without_all:cod_persona,num_documento,num_persona'                  
                ]);
            }
        });

        return $validator;
    }
}