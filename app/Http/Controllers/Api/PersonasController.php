<?php

namespace App\Http\Controllers\Api;

use App\Classes\Api\RunWS;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use GuzzleHttp\Client;
/**
 * Class TransferenciaTitulosController
 * @package App\Http\Controllers\Api
 *
 * @api {post} /transferencia-titulos Operaciones
 * @apiGroup Transferencia de Titulos
 * @apiDescription Este servicio permite realizar transferencias de titulos (nacionales e internacionales)
 * @apiVersion 0.1.0
 * @apiUse Login
 *
 * @apiParam {Number} cod_comitente_dde El código del comitente originario. En caso de no conocerlo, se deberá especificar el <code>num_comitente</code> originario
 * @apiParam {Number} [num_comitente_dde] El número del comitente originario. Debe especificarse <b>únicamente</b> en caso de no saber el cod_comitente originario
 *
 * @apiParam {Number} cod_comitente_hst El código del comitente destinatario. En caso de no conocerlo, se deberá especificar el <code>num_comitente</code> destinatario
 * @apiParam {Number} [num_comitente_hst] El número del comitente destinatario. Debe especificarse <b>únicamente</b> en caso de no saber el cod_comitente  destinatario
 *
 * @apiParam {Char} cod_accion La acción a realizar. <code>A</code>: alta | <code>M</code>: modificar | <code>D</code>: detalle | <code>E</code>: eliminar | <code>R</code>: restaurar
 * @apiParam {Number} cod_agente_depo_dde El código de agente originario
 * @apiParam {Number} cod_agente_depo_hst El código de agente destinario
 * @apiParam {String} fecha_concertacion La fecha de concertacion de la transferencia de titulos
 * @apiParam {String} fecha_liquidacion La fecha de liquidacion de la transferencia de titulos
 *
 * @apiParam {Number} [precio]
 * @apiParam {Number} [cod_pais_tit]
 * @apiParam {String} [observaciones]
 * @apiParam {Char} [es_transferencia_tit] Valores válidos: <code>Y</code> | <code>N</code>
 * @apiParam {Char} [se_informa_transf_cust] Valores válidos: <code>Y</code> | <code>N</code>
 * @apiParam {Char} [esta_emitida_transf_cust] Valores válidos: <code>Y</code> | <code>N</code>
 * @apiParam {Char} [es_manual] Valores válidos: <code>Y</code> | <code>N</code>
 * @apiParam {String} cod_tp_transferencia_tit_mov <code>EC</code> Recepción | <code>Envío</code
 *
 * @apiParam {Number} cantidad La cantidad de titulos a transferir
 * @apiParam {Array} titulos Objeto titulos
 * @apiParam {Number} titulos.cod_especie El código de la especie. En caso de no conocerlo, se deberá especificar la <code>abreviatura</code> de la especie
 * @apiParam {String} [titulos.abreviatura] La abreviatura de la especie. Debe especificarse <b>únicamente</b> en caso de no saber el cod_especie
 * @apiParam {Number} [titulos.cod_transferencia_tit_mov] Obligatorio en todos <u>menos</u> <code>A</code>
 * @apiParam {Number} titulos.cantidad La cantidad de titulos a transferir
 */

class PersonasController extends RunWS
{

    protected $resultado;

    public function run($base = null, Request $request)
    {

        $validator = $this->getValidator($request);
/*
        $data = (array) $resultado->getData();

        if (isset($data[0][0]->CodPersona)) {
            $cod_persona = $data[0][0]->CodPersona;    
        }
*/

        $num_documento="38928509";

        if($request->cod_accion = "D") {      

            $this->resultado =  $this->execute($request, $validator, $base, 'Personas', true, true); 
            $this->resultado = json_decode($this->resultado->getContent())[0][0];

            //Agregamos domicilios al array
            $domicilios[] = $this->execute($request, $validator, $base, 'PersonasDomicilios', true, true); 
            $this->resultado->domicilios = json_decode($domicilios[0]->getContent())[1];

            //Agregamos nacionalidades al array
            $nacionalidades[] = $this->execute($request, $validator, $base, 'PersonasNacionalidades', true, true); 
            $this->resultado->nacionalidades = json_decode($nacionalidades[0]->getContent())[2];

            //Agregamos nacionalidades al array
            $documentacion[] = $this->execute($request, $validator, $base, 'PersonasDocumentacion', true, true); 
            $this->resultado->documentacion = json_decode($documentacion[0]->getContent())[3];

            $this->resultado = response()->json($this->resultado, 200);
            //$this->resultado = json_encode($this->resultado);

        }
        

        if($request->cod_accion == "A" || $request->cod_accion == "M") {

            $this->resultado = $this->execute($request, $validator, $base, 'Personas', null, false);  

            if (isset($request->nacionalidades)) {
                $this->procesarNacionalidades($request, $validator, $base = true,  false);
            }

            if (isset($request->domicilios)) {
                $this->procesarDomicilios($request, $validator, $base = true,  false);
            }

            if (isset($request->documentacion)) {
                $this->procesarDocumentacion($request, $validator, $base = true,  true); 
            }              

        }
        

        return $this->resultado;
    }

    public function procesarNacionalidades($request, $validator, $base = null, $end_transaction=true) {
                
                foreach ($request->nacionalidades as $key => $nacionalidad) {

                    $persNacionalidad = [];
                    $persNacionalidad["username_db"] = $request->username_db;
                    $persNacionalidad["password_db"] = $request->password_db;
                    $persNacionalidad["num_documento"] = $request->num_documento;
                   
                    $persNacionalidad["cod_accion"] = $request->cod_accion;
                    $persNacionalidad["cod_pais"] = $nacionalidad["cod_pais"];
                    
                    
                    if (isset($nacionalidad["cod_persona_nacionalidad"])) {

                        $persNacionalidad["cod_persona_nacionalidad"] = $nacionalidad["cod_persona_nacionalidad"];   

                    }
                    
                    $persNacionalidad = (object) $persNacionalidad;

                    $this->resultado = $this->execute($persNacionalidad, $validator, $base, 'PersonasNacionalidades', null,  $end_transaction);

                }

    }

    public function procesarDomicilios($request, $validator, $base = null, $end_transaction=true) {
                $request2 = new Request();

                foreach ($request->domicilios as $key => $domicilio) {

                    $request2->merge([
                        'username_db' => $request->username_db,
                        'password_db' => $request->password_db,
                        //'cod_persona' => $cod_persona,
                        'num_documento' => $request->num_documento,
                        'cod_accion' => $request->cod_accion,
                        'cod_tp_domicilio' => $domicilio["cod_tp_domicilio"],
                        'cod_pais' => $domicilio["cod_pais"],
                        'cod_provincia' => $domicilio["cod_provincia"],
                        'calle' => $domicilio["calle"],
                        'altura_calle' => $domicilio["altura_calle"],
                        'piso' => $domicilio["piso"],
                        'departamento' => $domicilio["departamento"],
                        'codigo_postal' => $domicilio["codigo_postal"],
                        'localidad' => $domicilio["localidad"],
                        'telefono' => $domicilio["telefono"]
                    ]);


                    if(isset($domicilio["fax"])) {
                        $request2->merge(["fax" => $domicilio["fax"]]);
                    }


                    if(isset($domicilio["sector"])) {
                        $request2->merge(["sector" => $domicilio["sector"]]);
                    }

                    if(isset($domicilio["torre"])) {
                        $request2->merge(["torre" => $domicilio["torre"]]);
                    }

                    if(isset($domicilio["manzana"])) {
                        $request2->merge(["manzana" => $domicilio["manzana"]]);
                    }
                                          
                    
                    //$persDomicilios = (object) $persDomicilios;

                    $this->resultado = $this->execute($request2, $validator, $base, 'PersonasDomicilios', null,  $end_transaction);

                }

    }

    public function procesarDocumentacion($request, $validator, $base = null, $end_transaction=true) {
                $request3 = new Request();
                
                foreach ($request->documentacion as $key => $docu) {
                    
                    $request3->merge([
                        "username_db" => $request->username_db,
                        "password_db" => $request->password_db,
                        "num_documento" => $request->num_documento,
                        "cod_accion" => $request->cod_accion,
                        "cod_documentacion" => $docu["cod_documentacion"]
                    ]);

                    
                    if(isset($docu['fue_presentado'])) {
                        $request3->merge(["fue_presentado" => $docu["fue_presentado"]]);
                    }



                    if(isset($docu['fecha_vencimiento'])) {
                        $request3->merge(["fecha_vencimiento" => $docu["fecha_vencimiento"]]);
                    }


                    // Paso el objeto $request->documentacion a un array
                    $docuArray = (array)$request->documentacion;

                    //Si es la ultima vuelta del foreach pasar el parametro $end_transaction, sino pasarlo como false
                    if ($docu === end($docuArray)) {
                        $this->resultado = $this->execute($request3, $validator, $base, 'PersonasDocumentacion', null, $end_transaction); 
                    } else {
                        $this->resultado = $this->execute($request3, $validator, $base, 'PersonasDocumentacion', null, false);
                    }                               

                }

    }


    /**
     * Defino las validaciones
     *
     * @param $request
     * @return mixed
     */
    private function getValidator($request)
    {
        $validator = Validator::make($request->all(), [
            'username_db' => 'required|string',
            'password_db' => 'required|string'
        ]);

        // Valido que reciba lo minimo indispensable para saber que validaciones voy a ejecutar (codigo accion, titulos y tipo de transferencia)
        $validator->after(function ($validator) use ($request) {
            $validator->addRules([
                'cod_accion' => 'required|in:A,M,D,E,R'
            ]);
        });

        $validator->after(function ($validator) use ($request) {
            $validator->addRules([
                'cod_persona' => 'integer',
                'num_persona' => 'integer',
                'num_documento' => 'string',
                'nombre' => 'string',
                'apellido' => 'string',
                'descripcion' => 'string',
                'es_fisico' => 'in:Y,N',
                'cod_tipo_contribucion_iva' => 'string',
                'cod_documento_identidad' => 'string',
                'cod_sexo' => 'string',
                'fecha_ingreso' => 'date_format:"Ymd"',
                'fecha_nacimiento' => 'date_format:"Ymd"',
                'lugar_nacimiento' => 'string',
                'email' => 'string',
                'cuil' => 'string',
                'cuit' => 'string',
                'cdi' => 'string',
                'ruc' => 'string',
                'es_beneficiario' => 'in:Y,N',
                'es_cliente' => 'in:Y,N',
                'es_cliente_especial' => 'in:Y,N',
                'peps_obliga' => 'in:Y,N',
                'peps_baja' => 'in:Y,N',
                'no_presencial' => 'in:Y,N',
                'es_agente_recaudador' => 'in:Y,N',
                'num_inscripcion' => 'string',
                'es_sujeto_obligado' => 'in:Y,N',
                'codigo_fatca' => 'string',
                'id_fatca' => 'string',
                'es_inversor_calificado' => 'in:Y,N',
                'pertenece_lut' => 'in:Y,N',
                'cod_perfiles_eco_fin' => 'string',
                'es_extranjero' => 'in:Y,N',
                'cod_actividad' => 'string',
                'cod_pais_nacionalidad' => 'string',
                'cod_tipo_ingresos_brutos' => 'string',
                'cod_usuario_auditoria' => 'string',
                'domicilios' => 'array'
            ]);
        });

        // Agrego las validaciones exclusivas para ALTA o MODIFICACION
        $validator->after(function ($validator) use ($request) {
            if ($request->cod_accion == "A" || $request->cod_accion == "M") {
                $validator->addRules([
                    'num_documento' => 'string|string|min:1|required_without_all:cod_persona',
                    'nombre' => 'required|string',
                    'apellido' => 'required|string',
                    'descripcion' => 'required|string',
                    'es_fisico' => 'required|in:Y,N',
                    'cod_tipo_contribucion_iva' => 'required|string',
                    'cod_documento_identidad' => 'required|string',
                    'cod_sexo' => 'required|string',
                    'fecha_ingreso' => 'required|date_format:"Ymd"',
                    'email' => 'required|string',
                    'fecha_nacimiento' => 'required|date_format:"Ymd"',
                    'domicilios' => 'required|array'
                ]);
            }
        });

        // Agrego las validaciones exclusivas para DETALLE, ELIMINAR Y RESTORE
        $validator->after(function ($validator) use ($request) {
            if ($request->cod_accion == "D" || $request->cod_accion == "E" || $request->cod_accion == "R") {
                $validator->addRules([
                    'cod_persona' => 'integer|integer|min:1|required_without_all:num_documento,num_persona',
                    'num_documento' => 'string|string|min:1|required_without_all:cod_persona,num_persona',
                    'num_persona' => 'integer|integer|min:1|required_without_all:cod_persona,num_documento'                
                ]);
            }
        });

        return $validator;
    }
}