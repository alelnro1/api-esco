<?php

namespace App\Http\Controllers\Api;

use App\Classes\Api\RunWS;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

/**
 * Class TransferenciaTitulosController
 * @package App\Http\Controllers\Api
 *
 * @api {post} /transferencia-titulos Operaciones
 * @apiGroup Transferencia de Titulos
 * @apiDescription Este servicio permite realizar transferencias de titulos (nacionales e internacionales)
 * @apiVersion 0.1.0
 * @apiUse Login
 *
 * @apiParam {Number} cod_comitente_dde El código del comitente originario. En caso de no conocerlo, se deberá especificar el <code>num_comitente</code> originario
 * @apiParam {Number} [num_comitente_dde] El número del comitente originario. Debe especificarse <b>únicamente</b> en caso de no saber el cod_comitente originario
 *
 * @apiParam {Number} cod_comitente_hst El código del comitente destinatario. En caso de no conocerlo, se deberá especificar el <code>num_comitente</code> destinatario
 * @apiParam {Number} [num_comitente_hst] El número del comitente destinatario. Debe especificarse <b>únicamente</b> en caso de no saber el cod_comitente  destinatario
 *
 * @apiParam {Char} cod_accion La acción a realizar. <code>A</code>: alta | <code>M</code>: modificar | <code>D</code>: detalle | <code>E</code>: eliminar | <code>R</code>: restaurar
 * @apiParam {Number} cod_agente_depo_dde El código de agente originario
 * @apiParam {Number} cod_agente_depo_hst El código de agente destinario
 * @apiParam {String} fecha_concertacion La fecha de concertacion de la transferencia de titulos
 * @apiParam {String} fecha_liquidacion La fecha de liquidacion de la transferencia de titulos
 *
 * @apiParam {Number} [precio]
 * @apiParam {Number} [cod_pais_tit]
 * @apiParam {String} [observaciones]
 * @apiParam {Char} [es_transferencia_tit] Valores válidos: <code>Y</code> | <code>N</code>
 * @apiParam {Char} [se_informa_transf_cust] Valores válidos: <code>Y</code> | <code>N</code>
 * @apiParam {Char} [esta_emitida_transf_cust] Valores válidos: <code>Y</code> | <code>N</code>
 * @apiParam {Char} [es_manual] Valores válidos: <code>Y</code> | <code>N</code>
 * @apiParam {String} cod_tp_transferencia_tit_mov <code>EC</code> Recepción | <code>Envío</code
 *
 * @apiParam {Number} cantidad La cantidad de titulos a transferir
 * @apiParam {Array} titulos Objeto titulos
 * @apiParam {Number} titulos.cod_especie El código de la especie. En caso de no conocerlo, se deberá especificar la <code>abreviatura</code> de la especie
 * @apiParam {String} [titulos.abreviatura] La abreviatura de la especie. Debe especificarse <b>únicamente</b> en caso de no saber el cod_especie
 * @apiParam {Number} [titulos.cod_transferencia_tit_mov] Obligatorio en todos <u>menos</u> <code>A</code>
 * @apiParam {Number} titulos.cantidad La cantidad de titulos a transferir
 */

class TransferenciaTitulosController extends RunWS
{
    public function run($base = null, Request $request)
    {
        $validator = $this->getValidator($request);

        $ids = [];

        foreach ($request->titulos as $key => $titulo) {
            $ret_sp = $this->execute($request, $validator, $base, 'TransferenciaTitulos', $key);

            $id = json_decode($ret_sp->getContent())[$key][0]->CodTransferenciaTitMov;

            array_push($ids, $id);
        }

        return $ids;
    }

    /**
     * Defino las validaciones
     *
     * @param $request
     * @return mixed
     */
    private function getValidator($request)
    {
        $validator = Validator::make($request->all(), [
            'username_db' => 'required|string',
            'password_db' => 'required|string'
        ]);

        // Valido que reciba lo minimo indispensable para saber que validaciones voy a ejecutar (codigo accion, titulos y tipo de transferencia)
        $validator->after(function ($validator) use ($request) {
            $validator->addRules([
                'titulos' => 'required|array',
                'cod_accion' => 'required|in:A,M,D,E,R',
                'cod_tp_transferencia_tit_mov' => 'required|string',
            ]);
        });

        $validator->after(function ($validator) use ($request) {
            $validator->addRules([
                'precio' => 'integer',
                'cod_pais_tit' => 'integer',
                'observaciones' => 'string',
                'es_transferencia_tit' => 'in:Y,N',
                'se_informa_transf_cust' => 'in:Y,N',
                'esta_emitida_transf_cust' => 'in:Y,N',
                'es_manual' => 'in:Y,N'
            ]);
        });

        // Si es un envio el "cod comitente desde" es obligatorio
        $validator->after(function ($validator) use ($request) {
            if ($request->cod_tp_transferencia_tit_mov == "ED") {
                $validator->addRules([
                    'cod_comitente_dde' => 'integer|required_without:num_comitente_dde|empty_when:num_comitente_dde',
                    'num_comitente_dde' => 'integer|required_without:cod_comitente_dde|empty_when:cod_comitente_dde'
                ]);
            }
        });

        // Si es una recepcion el "cod comitente hasta" es obligatorio
        $validator->after(function ($validator) use ($request) {
            if ($request->cod_tp_transferencia_tit_mov == "EC") {
                $validator->addRules([
                    'cod_comitente_hst' => 'integer|required_without:num_comitente_hst',
                    'num_comitente_hst' => 'integer|required_without:cod_comitente_hst'
                ]);
            }
        });

        // Agrego las validaciones exclusivas para ALTA o MODIFICACION
        $validator->after(function ($validator) use ($request) {
            if ($request->cod_accion == "A" || $request->cod_accion == "M") {
                $validator->addRules([
                    'cod_agente_depo_dde' => 'required|integer',
                    'cod_agente_depo_hst' => 'required|integer',
                    'fecha_concertacion' => 'required|date_format:"Ymd"',
                    'fecha_liquidacion' => 'required|date_format:"Ymd"',

                    'titulos.*.cod_especie' => 'integer|integer|min:1|required_without:titulos.*.abreviatura',
                    'titulos.*.abreviatura' => 'string|required_without:titulos.*.cod_especie',
                    'titulos.*.cantidad' => 'required|integer|min:1',
                ]);
            }
        });

        // Agrego las validaciones exclusivas para DETALLE, ELIMINAR Y RESTORE
        $validator->after(function ($validator) use ($request) {
            if ($request->cod_accion == "D" || $request->cod_accion == "E" || $request->cod_accion == "R") {
                $validator->addRules([
                    'titulos.*.cod_especie' => 'integer|empty_when:titulos.*.abreviatura',
                    'titulos.*.abreviatura' => 'string|empty_when:titulos.*.cod_especie',

                    'titulos.*.cod_transferencia_tit_mov' => 'required|integer',
                    'titulos.*.cantidad' => 'required|integer',
                ]);
            }
        });

        return $validator;
    }
}