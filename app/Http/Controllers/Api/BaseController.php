<?php

namespace App\Http\Controllers\Api;

use App\Classes\OTF;
use App\Http\Controllers\Controller;
use App\Traits\ErroresSQL;
use Illuminate\Database\QueryException;

class BaseController extends Controller
{
    use ErroresSQL;

    protected static $instance;

    protected $db;

    /**
     * Contendrá todos los resultados de las operaciones para despues
     * recorrerlos y devolver un OK o un ERROR
     *
     * @var
     */
    public $objetos_resultados = [];

    public static function getInstance()
    {
        if (is_null(static::$instance)){
            static::$instance = new static();
        }
        return static::$instance;
    }

    /**
     * Me conecto con la base de datos, validando que escribieron bien
     * el usuario y la contraseña
     *
     * @param $request
     * @return bool|\Illuminate\Database\Connection|\Illuminate\Http\JsonResponse
     */
    public function conectarDB($request)
    {
        $this->db = $this->probarConexion($request->username_db, $request->password_db);

        if (!$this->db) {
            $this->db = false;
        } else {
            $this->db->beginTransaction();
        }

        return $this->db;
    }

    /**
     * Nos desconectamos de la base y si no hay ningun error, hacemos un commit,
     * de lo contrario hacemos un rollBack
     *
     *
     * @param bool $exception
     * @param null $mensajes_de_error
     * @return \Illuminate\Http\JsonResponse
     */
    public function desconectarDB($exception = false, $mensajes_de_error = null)
    {
        

        // Se cortó la ejecución por una excepcion
        if ($exception) {

            return response()->json($exception);
            $this->db->rollBack();

            if (!$mensajes_de_error) {
                $mensajes_de_error = ['errores' => 'Error en los datos enviados.'];
            }

            return response()->json($mensajes_de_error, 422);
        }

        // Inicializo variable que indica si hubo algun en los resultados
        $error = false;

        foreach ($this->objetos_resultados as $key => $objetos) {
            foreach ($objetos as $objeto) {
                $errores = $this->getErrores($objeto);

                if ($errores) {
                    $this->db->rollBack();

                    return response()->json([
                        'errores' => $errores
                    ], 422);
                }
            }
        }

        if (!$error) {
            $this->db->commit();
            return response()->json($this->objetos_resultados, 200);
        }
        // Agarro todos los objetos_resultados y me fijo si alguno de ellos tiene Status = Error
        // En ese caso hago un rollback y muestro el error que generó el status = error
        // Caso contrario hago un commit
    }

    /**
     * Probamos la conexion con la base de datos recibiendo como parametros
     * de la API el usuario y contraseña de la base
     *
     * @param $username_db
     * @param $password_db
     * @return bool|\Illuminate\Database\Connection
     *
     * @apiDefine Login
     * @apiHeader {String} Accept application/json
     * @apiHeaderExample {json} Header-Example:
     *     {
     *       "Accept": "application/json"
     *     }
     *
     * @apiParam {String} username_db Usuario para la conexión a la base de datos
     * @apiParam {String} password_db Contraseña para la conexión a la base de datos
     *
     * @apiErrorExample {json} Error-Ausencia-Datos-Login:
     *     HTTP/1.1 422 Unprocessable Entity
     *     {
     *       "error": "The given data failed to pass validation."
     *     }
     * @apiErrorExample {json} Error-Login:
     *     HTTP/1.1 401 Unauthorized
     *     {
     *       "error": "Login invalido."
     *     }
     */
    public function probarConexion($username_db, $password_db)
    {
        // Definimos los parametros de conexion
        $database = new OTF([
            'driver' => env('DB_BOLSA_DRIVER'),
            'database' => env('DB_BOLSA_DATABASE'),
            'host' => env('DB_BOLSA_HOST'),
            'port' => env('DB_BOLSA_PORT'),
            'username' => $username_db,
            'password' => $password_db
        ]);

        // Obtenemos el objeto de conexxion
        $connection = $database->getConnection();

        try {
            $connection->select('SELECT 1 as ID');
        } catch (QueryException $e) {
            return false;
        }

        return $connection;
    }

    /**
     * Agrego un nuevo resultado obtenido de SQL para despues recorrerlos a todos
     * y ver si hubo algun error para hacer un rollback o sino, un commit
     *
     * @param $resultado
     */
    public function agregarNuevoResultado($resultado)
    {

            array_push($this->objetos_resultados, $resultado);    
        

    }
}