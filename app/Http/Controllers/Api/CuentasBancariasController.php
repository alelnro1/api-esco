<?php

namespace App\Http\Controllers\Api;

use App\Classes\Api\RunWS;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

/**
 * Class CuentasBancariasController
 * @package App\Http\Controllers\Api
 *
 * @api {post} /cuenta-bancaria Operaciones
 * @apiGroup Cuentas Bancarias
 * @apiDescription Este servicio permite realizar las siguiente operaciones sobre <b>Cuentas Bancarias</b>: alta, modificación, detalle, eliminar y restaurar
 * @apiVersion 0.1.0
 * @apiUse Login
 *
 * @apiParam {Char} cod_accion La acción a realizar. <code>A</code>: alta | <code>M</code>: modificar | <code>D</code>: detalle | <code>E</code>: eliminar | <code>R</code>: restaurar
 * @apiParam {Number} cod_comitente El código del comitente. En caso de no conocerlo, se deberá especificar el <code>num_comitente</code>
 * @apiParam {Number} [num_comitente] El número del comitente. Debe especificarse <b>únicamente</b> en caso de no saber el cod_comitente
 *
 * @apiParam {Number} cod_moneda El código de la moneda. En caso de no saberlo, se deberá especificar el <code>cod_interfaz_moneda</code>
 * @apiParam {String} [cod_interfaz_moneda] El código de interfaz de la moneda. Se deberá especificar <b>únicamente</b> en caso de no conocer el <code>cod_moneda</code>
 *
 * @apiParam {Number} cod_ent_liquidacion El código del banco
 * @apiParam {Number} cod_tp_cta_bancaria El código del Tipo de Cuenta Bancaria
 * @apiParam {String} fecha_apertura La fecha de apertura. Deberá estar en formato <code>Ymd</code>
 * @apiParam {String} num_subcuenta_mon El número de la Cuenta Bancaria
 *
 * @apiParam {Number} cod_cmt_cta_ent_liquidacion Se deberá enviar en todas las solicitudes que <b>no</b> sean <u><b>alta</b></u>
 * @apiParam {String} [cbu] El CBU de la Cuenta Bancaria
 * @apiParam {String} [alias] El Alias de la Cuenta Bancaria
 * @apiParam {String} [cuit_titular] El CUIT del titular de la Cuenta Bancaria
 * @apiParam {Number} [num_sucursal] El número de sucursal donde está radicada la Cuenta Bancaria
 * @apiParam {Char} [es_liquidadora] Valores válidos: <code>Y</code> | <code>N</code>
 * @apiParam {Char} [es_default] Valores válidos: <code>Y</code> | <code>N</code>
 * @apiParam {Char} [usa_param_ent_liq] Valores válidos: <code>Y</code> | <code>N</code>
 * @apiParam {Char} [permite_parc] Valores válidos: <code>Y</code> | <code>N</code>
 * @apiSuccessExample {json} Success-Response:
 *  HTTP/1.1 200 OK
 *  [
 *      [
 *           {
 *               "Status": "Ok",
 *               "Mensaje": "",
 *               "CodCmtCtaEntLiquidacion": "24399"
 *           }
 *      ]
 *  ]
 * @apiErrorExample {json} Error-Response:
 *  HTTP/1.1 422 Unprocessable Entity
 *  {
 *      "errores": [
 *          "(#ERRA003) No existe el Comitente"
 *      ]
 *  }
 *
 */
class CuentasBancariasController extends RunWS
{
    /**
     * Mando a ejecutar el webservice
     *
     * @param null $base
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function run($base = null, Request $request)
    {


        $validator = $this->getValidator($request);

        return $this->execute($request, $validator, $base, 'CuentaBancaria');
    }

    /**
     * Defino las validaciones
     *
     * @param $request
     * @return mixed
     */
    private function getValidator($request)
    {
        $validator = Validator::make($request->all(), [
            'username_db' => 'required|string',
            'password_db' => 'required|string'
        ]);

        $validator->after(function ($validator) use ($request) {
            $validator->addRules([
                'cod_accion' => 'required|in:A,M,D,E,R',

                'cod_comitente' => 'integer|required_without:num_comitente|empty_when:num_comitente',
                'num_comitente' => 'integer|required_without:cod_comitente|empty_when:cod_comitente',

                'cod_cmt_cta_liquidacion' => 'integer',
                'cod_tp_cta_bancaria' => 'integer',
                'cod_mo neda' => 'integer',
                'num_sub_cuenta_mon' => 'string',

                'cbu' => 'string',
                'alias' => 'string',
                'cuit_titular' => 'regex:/^\d{2}\-\d{8}\-\d{1}$/u',
                'num_sucursal' => 'integer',
                'es_liquidadora' => 'in:Y,N',
                'es_default' => 'in:Y,N',
                'usa_param_ent_liq' => 'in:Y,N',
                'permite_parc' => 'in:Y,N'
            ]);
        });

        $validator->after(function ($validator) use ($request) {
            // Validamos que se reciba el cod de cuenta bancaria siempre y cuando no sea un ALTA
            if ($request->cod_accion != "A") {
                $validator->addRules([
                    'cod_cmt_cta_liquidacion' => 'required|integer'
                ]);
            }

            // Validamos que nos pasen los datos necesarios cuanto es un ALTA o una MODIFICACION
            if ($request->cod_accion == "A" || $request->cod_accion == "M") {
                $validator->addRules([
                    'cod_interfaz_moneda' => 'required_without:cod_moneda|empty_when:cod_moneda',
                    'cod_moneda' => 'required_without:cod_interfaz_moneda|empty_when:cod_interfaz_moneda',

                    'cod_ent_liquidacion' => 'required',
                    'cod_tp_cta_bancaria' => 'required|integer',
                    'fecha_apertura' => 'required|string',
                    'num_sub_cuenta_mon' => 'required|string'
                ]);
            }
        });

        return $validator;
    }
}