<?php

namespace App\Http\Controllers\Api;

use App\Classes\Api\RunWS;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

/**
 * Class ComitentesController
 * @package App\Http\Controllers\Api
 *
 * @api {post} /comitente Operaciones
 * @apiGroup Comitente
 * @apiDescription Este servicio permite realizar las siguiente operaciones sobre <b>Comitentes</b>: alta, modificación, detalle, eliminar y restaurar
 * @apiVersion 0.1.0
 * @apiUse Login
 *
 * @apiParam {Char} cod_accion La acción a realizar. <code>A</code>: alta | <code>M</code>: modificar | <code>D</code>: detalle | <code>E</code>: eliminar | <code>R</code>: restaurar
 * @apiParam {Number} cod_comitente El código del comitente a operar
 * @apiParam {Number} cod_grupo_ar_opers_burs El código de operadora
 * @apiParam {String} cod_tp_contrib_retencion El código del tipo de contribuyente. EJ: extranjero | EX: exento | IN: inscripto | NI: no inscripto
 * @apiParam {Number} cod_grupo_acr El código de grupo accedor
 * @apiParam {String} descripcion La descripción del comitente
 * @apiParam {Char} es_fisico Valores válidos: <code>Y</code> | <code>N</code>
 * @NumComitente
 * @apiParam {Number} cod_perfil_cmt El código del perfil del comitente
 *
 * @apiParam {String} [fecha_ingreso] La fecha de alta. <i>Sólo informar si es distinta a hoy</i>
 * @apiParam {String} [cod_tp_afjp_cmt] El código AFJP del comitente
 * @apiParam {Number} [nivel_seguridad] El nivel de seguridad. <i>Sólo informar si es distinto de 0</i>
 * @apiParam {Number} [cod_grupo_eco] El código del grupo económico
 * @apiParam {String} [cod_tp_manejo_cart] El código del tipo de manejo de cargar. <i>Sólo informar cuando es distinto a <b>No Discrecional</b></i>
 * @apiParam {Number} [cod_canal_vta] El código de canal de venta
 * @apiParam {Number} [cod_categoria] El código de la categoría
 * @apiParam {Char} [requiere_firma_conjunta] Valores válidos: <code>Y</code> | <code>N</code>
 * @apiParam {String} [referencia_firma_conjunta]
 * @apiParam {Char} [es_mercado] Valores válidos: <code>Y</code> | <code>N</code>
 * @apiParam {Char} [es_cartera_propia] Valores válidos: <code>Y</code> | <code>N</code>
 * @apiParam {Number} [cod_tp_cmt_trading] El código de tipo de trading. <i>Sólo informar cuando es distinto a <b>5</b></i>
 * @apiParam {Number} [cod_agente] El código de agente
 * @apiParam {Number} [cod_cuotapartista] El código del cuotapartista
 * @apiParam {String} [contacto] La dirección del contacto del comitente
 * @apiParam {String} [observaciones]
 * @apiParam {String} [emails_info] Los emails del comitente, separados por <code>;</code>
 * @apiParam {Char} [no_presencial] Valores válidos: <code>Y</code> | <code>N</code>
 * @apiParam {Char} [fondos_terceros] Valores válidos: <code>Y</code> | <code>N</code>
 * @apiParam {Number} [cod_categoria_uif] El código de la categoría UIF
 * @apiParam {Char} [es_asig_cat_uif_manual] Valores válidos: <code>Y</code> | <code>N</code>
 * @apiParam {Char} [es_institucional] Valores válidos: <code>Y</code> | <code>N</code>
 * @apiParam {Char} [cod_pri] Valores válidos: <code>Y</code> | <code>N</code>
 *
 * @apiSuccessExample {json} Success-Response:
 *  HTTP/1.1 200 OK
 *  [
 *      [
 *           {
 *               "Status": "Ok",
 *               "Mensaje": "",
 *               "CodComitente": "18223"
 *           }
 *      ]
 *  ]
 *
 * @apiErrorExample {json} Error-Response:
 *  HTTP/1.1 422 Unprocessable Entity
 *  {
 *      "errores": [
 *          "(#ERRA0002) El Numero de Comitente ya existe para otro Registro",
 *          "(#ERRA0004) La descripcion ya existe para otro registro"
 *      ]
 *  }
 */
class ComitentesController extends RunWS
{
    /**
     * Mando a ejecutar el webservice
     *
     * @param null $base
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function run($base = null, Request $request)
    {
        $validator = $this->getValidator($request);

        return $this->execute($request, $validator, $base, 'Comitente');
    }

    protected function getValidator($request)
    {
        $validator = Validator::make($request->all(), [
            'username_db' => 'required|string',
            'password_db' => 'required|string'
        ]);

        $validator->after(function ($validator) use ($request) {
            $validator->addRules([
                'cod_accion' => 'required|string|in:A,M,E,R,D',
                'cod_comitente' => 'integer',

                'cod_grupo_ar_opers_burs' => 'required|integer',
                'cod_tp_contrib_retencion' => 'required|string',
                'cod_grupo_acr' => 'required|integer',
                'descripcion' => 'required|string',
                'es_fisico' => 'required|in:Y,N',
                'num_comitente' => 'required|integer',
                'fecha_ingreso' => 'required|date_format:"Ymd"',

                'cod_tp_afjp_cmt' => 'string',
                'nivel_seguridad' => 'integer',
                'cod_grupo_eco' => 'integer',
                'cod_tp_manejo_cart' => 'string',
                'cod_canal_vta' => 'integer',
                'cod_categoria' => 'integer',
                'require_firma_conjunta' => 'in:Y,N',
                'referencia_firma_conjunta' => 'string',
                'es_mercado' => 'in:Y,N',
                'es_cartera_propia' => 'in:Y,N',
                'cod_tp_cmt_trading' => 'integer',

                'cod_agente' => 'integer',
                'cod_cuotapartista' => 'integer',
                'contacto' => 'string',
                'observaciones' => 'string',
                'emails_info' => 'string', //Validar que los mails sean validos entre los ;
                'no_presencial' => 'in:Y,N',
                'fondos_terceros' => 'in:Y,N',
                'cod_categoria_uif' => 'integer',
                'es_asign_cat_uif_manual' => 'in:Y,N',
                'es_institucional' => 'in:Y,N',
                'cod_pri' => 'in:Y,N'
             ]);
        });

        return $validator;
    }
}