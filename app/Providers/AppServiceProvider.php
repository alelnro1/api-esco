<?php

namespace App\Providers;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('empty_when', function ($attribute, $value, $parameters, $validator) {
            foreach ($parameters as $key)
            {
                if ( ! empty(Input::get($key)))
                {
                    return false;
                }
            }

            return true;
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        /*Validator::extend('empty_when', function ($attribute, $value, $parameters, $validator) {
            die($attribute);
            foreach ($parameters as $key)
            {
                if ( ! empty(Input::get($key)))
                {
                    return false;
                }
            }

            return true;
        });*/
    }
}
