<?php

namespace App\Traits;

trait ErroresSQL
{
    /**
     * Devuelvo una respuesta parseada en JSON con errores (si los hubo)
     *
     * @param $resultado
     * @return \Illuminate\Http\JsonResponse
     */
    public function getErrores($objeto)
    {
        // Existe el atributo STATUS => Posible error
        if (isset($objeto->Status)) {

            // Existe un error
            if ($objeto->Status === "Error") {

                // Separo los errores devueltos por SQL
                $mensajes = explode(";", $objeto->Mensaje);

                // Inicializo el array de errores para mostrar como json
                $errores = [];

                foreach ($mensajes as $mensaje) {
                    if (!empty($mensaje)) {
                        array_push($errores, $mensaje);
                    }
                }

                return $errores;
            } else {
                return false;
            }
        }
    }
}