<?php

namespace App\Traits;

trait ParsearValores
{
    /**
     * Si el valor esta vacio o no esta seteado enviamos NULL
     * Si hay algun valor, y está seteado como texto vamos a devolver entre comillas
     *
     * @param $valor
     * @param bool $texto
     * @param char $valor_requerido El valor que debe recibirse para enviarse a ESCO
     * @return string
     */
    public function parsearValor($valor, $texto = false, $valor_requerido = null)
    {
        if ($valor === "" || !isset($valor)) {
            $res = 'NULL';
        } else {
            // Si existe el valor requerido es porque pidieron Y o N => es texto
            if ($valor_requerido && $valor == $valor_requerido) {
                $res = "'" . $valor . "'";
            } else if ($valor_requerido === null) {
                // No hay valor requerido => Verifico si es texto
                if ($texto) {
                    $res = "'" . $valor . "'";
                } else {
                    $res = $valor;
                }
            } else {
                return 'NULL';
            }
        }

        return $res;
    }

    /**
     * Devuelvo un string con los parametros opcionales para no enviar todo al SP
     *
     * @param $valores
     * @return string
     */
    public function armarValoresOpcionales($valores)
    {
        // Inicializo la variable de resultado
        
        $res = "";

        foreach ($valores as $campo_base => $valor) {
            if ($valor != "NULL") {
                $res .= "@" . $campo_base . "=" . $valor . ",";
            }
        }

        return $res;
    }
}