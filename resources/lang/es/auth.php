<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    
    'failed'                                       => 'Estas credenciales no coinciden con nuestros registros.',
    'throttle'                                     => 'Demasiados intentos de inicio de sesión. Vuelva a intentarlo en :seconds segundos.',
    
    'physical_person'                              => '{0} Persona Física|[1,*] Personas Físicas',
    'legal_person'                                 => '{0} Persona Jurídica|[1,*] Personas Jurídicas',
    
    /* Legal Persons Signup */
    'legal_person_data'                            => 'Datos de la Persona Jurídica',
    'authorizations'                               => 'Autorizaciones',
    'authorized'                                   => 'Autorizado',
    'expiry_date'                                  => 'Vto. Mandato',
    'public_registry_registration'                 => 'Inscripción en el registro público',
    'signatory'                                    => 'Firmante',
    'sworn_declaration_politically_exposed_person' => 'Declaración Jurada sobre la condición de persona expuesta politicamente',
    'sworn_declaration_us_corporate'               => 'Declaración jurada sobre la condición de U.S. Corporate',
    'if_afirmative_position'                       => '(En caso afirmativo) CARGO / FUNCION / JERARQUIA / RELACION',
    'ddjj_subject_bound'                           => 'DDJJ Sujeto Obligado (RES. UIF 03/2014)',
    
    /* Physical Persons Signup */
    'comitent_data'                                => 'Datos de los Comitentes',
    'spouse_data'                                  => 'Datos del Conyuge',
    'holder'                                       => 'Titular',
    
    /* Common signup fields */
    'personal_data'                                => 'Datos Personales',
    'address'                                      => 'Domicilio',
    'personal_address'                             => 'Domicilio Particular',
    'name'                                         => '{0} Nombre|[1,*] Nombres',
    'last_name'                                    => '{0} Apellido|[1,*] Apellidos',
    'birthday'                                     => 'Fecha de Nacimiento',
    'nationality'                                  => 'Nacionalidad',
    'other_nationality'                            => 'Otra Nacionalidad',
    'gender'                                       => 'Sexo',
    'born_place'                                   => 'Lugar de Nacimiento',
    'country_residence'                            => 'País de Residencia Fiscal',
    'legal_id'                                     => 'N° de Indetif. Fiscal en ese país',
    'document_type_and_number'                     => 'Tipo y N° Documento',
    'cuit_cuil'                                    => 'CUIT / CUIL',
    'cuit_cuil_cdi'                                => 'CUIT / CUIL / CDI',
    'iva_condition'                                => 'Condición ante el IVA',
    'earnings_condition'                           => 'Condicion ante Ganancias',
    'civil_state'                                  => 'Estado Civil',
    'main_activity'                                => 'Actividad Principal',
    'employment_relationship'                      => 'Relación Laboral',
    'company'                                      => 'Empresa',
    'position'                                     => 'Cargo',
    'origin_of_values'                             => 'Origen de los valores',
    'email'                                        => 'Mail',
    'permanent_instructions'                       => 'Instrucciones Permanentes',
    
    /* Home fields */
    'street'                                       => 'Calle - N° - Piso - Depto',
    'zip_code'                                     => 'Codigo Postal / Localidad',
    'state'                                        => 'Provincia / Pais',
    'personal_phone'                               => 'Telefono Particular',
    'cellphone'                                    => 'Celular',
    
    /* Legal Person Fields */
    'business_name'                                => 'Razón Social',
    'cuit'                                         => 'CUIT',
    'method_of_income_values'                      => 'Modo de Ingreso de Valores',
    'dni_le_lc'                                    => 'DNI / LE / LC',
    
    /* Legal Person Public Registry */
    'place'                                        => 'Lugar',
    'date'                                         => 'Fecha',
    'number'                                       => 'Número',
    'book'                                         => 'Libro',
    'volume'                                       => 'Tomo',
    'tax_sheet'                                    => 'Folio',
    
    /* Legal Person Sworn Data */
    'included_within_uif_nomination'               => 'Incluido dentro de la nómina UIF',
    
    /* Legal Permanent Instruction Data */
    'bank_account'                                 => 'Cuenta Bancaria',
    'pesos'                                        => 'Pesos',
    'dollars'                                      => 'Dolares',
    'bank'                                         => 'Banco',
    'account_number'                               => 'Numero de Cuenta',
    'account_type'                                 => 'Tipo de Cuenta',
    'cbu'                                          => 'CBU',
    'cuit_cuil_bank_account_owner'                 => 'CUIT / CUIL del Titular de la Cuenta Bancaria',
    
    /* Legal Subject Bound */
    'sworn_declaration_obligated_subject'          => 'SUJETO OBLIGADO (de acuerdo a lo establecido en la Ley N° 25.246 y sus modificatorias)',
    
    /* Sworn US Corporate */
    'included_in_us_corporate'                     => 'Incluido dentro del concepto de U.S. Corporate',
    'if_afirmative_motive'                         => '(En caso afirmativo) Indicar detalladamente el motivo',
    
    /* Form of Owner */
    'form_of_owner'                                => 'Formulario de Beneficiario / Beneficiario Final',
    'owner_type'                                   => 'Tipo de Propietario',
    'legal_person_physical_denomination_name'      => 'Persona Juridica - Denominación Persona Física Apellido y Nombre',
    'owner_nacionality'                            => 'Nacionalidad del Propietario',
    'owner_birthday'                               => 'Fecha de Nacimiento del Propietario',
    'owner_phone'                                  => 'N° de Teléfono del Propietario',
    'owner_address'                                => 'Domicilio del Propietario',
    'owner_cuit_cuil_cdi'                          => 'CUIT/CUIL/CDI del Propietario',
    'owner_property_percentage'                    => '% de Propiedad',
    'owner_direct'                                 => 'Directo',
    'owner_indirect'                               => 'Indirecto',
    
    /* Register user */
    'dni'                                          => 'DNI',
    'userName'                                     => 'Usuario',
    'comitente'                                    => 'Comitente',
    'register'                                     => 'Registrarse',
    'register_title'                               => 'Alta nuevo usuario'

];
