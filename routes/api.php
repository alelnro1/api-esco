<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['namespace' => 'Api'], function () {
    Route::post('cuenta-bancaria', 'CuentasBancariasController@run');
    Route::post('transferencia-titulos', 'TransferenciaTitulosController@run');
    Route::post('comitente', 'ComitentesController@run');
    Route::post('perfil-comitente', 'PerfilesController@run');
    Route::post('personas', 'PersonasController@run');
    Route::post('personas-domicilios', 'PersonasDomiciliosController@run');
    Route::post('personas-nacionalidades', 'PersonasNacionalidadesController@run');
    Route::post('personas-documentacion', 'PersonasDocumentacionController@run');
});
